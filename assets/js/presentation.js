window.Vue = require('vue');
Vue.component('presentation', require('./components/presentation.vue'));

const electron = require('electron');

const vueApp = new Vue({
	el: '#app',
	data: {
		'slides': electron.remote.getGlobal('shared').slides,
		'currentSlide': 1
	}
});
