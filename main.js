'use strict';

if(require('electron-squirrel-startup')) return;

const electron = require('electron');

const app = electron.app;

require('electron-debug')();

// prevent window being garbage collected
let mainWindow;

global.shared = {
	file: null,
	slides: {}
};

function onClosed() {
	// dereference the window
	// for multiple windows store them in an array
	mainWindow = null;
}

function createMainWindow() {
	const {width, height} = electron.screen.getPrimaryDisplay().workAreaSize;
	const win = new electron.BrowserWindow({width: width, height: height, show: false});

	win.loadURL(`file://${__dirname}/index.html`);
	win.on('closed', onClosed);

	win.once('ready-to-show', () => {
		win.show()
	});

	return win;
}

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	if (!mainWindow) {
		mainWindow = createMainWindow();
	}
});

app.on('ready', () => {
	mainWindow = createMainWindow();
});
